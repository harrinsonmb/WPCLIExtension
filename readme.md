# Instalación de WordPress CLI en Azure

1.  Entrar en el app service desde portal.azure.com

![portal.azure.com](assets/img/image_01.png)

2.  En el apartado "Herramientas de desarrollo" hacer clic en **Extensiones**

![Herramientas de desarollo](assets/img/image_02.png)

3.  Hacer clic en **+ Agregar**

![Agregar](assets/img/image_03.png)

4.  Buscar la extensión llamada "WordPress CLI" y hacer clic en el resultado

![WordpPress CLI](assets/img/image_04.png)

5.  Hacer clic en **Aceptar**

![Aceptar](assets/img/image_05.png)

6.  De nuevo, hacer clic en **Aceptar**

![Aceptar](assets/img/image_06.png)

7.  Ahora en las notificaciones, verás como se instala la extensión

![Instalando extensión](assets/img/image_07.png)

8.  Una vez finalizado serás notificado de ello.

![Extensión instalada](assets/img/image_08.png)

9.  Ahora debes reiniciar la aplicación, para ello vuelve a "Introducción" y haz clic en **Reiniciar** y luego en **Sí**

![Reiniciar app service](assets/img/image_09.png)

10. Una vez reiniciado el app service, serás notificado de ello

![App service reiniciado](assets/img/image_10.png)

11. Ahora, desde el apartado "Herramientas de desarrollo" haremos clic en **Herramientas avanzadas** y luego en **Ir**

![Herramientas avanzadas -> Ir](assets/img/image_11.png)

12. En el menú superior de la nueva ventana, poner el cursor sobre "Debug console" y hacer clic en **PowerShell**

![Debug console -> Powershell](assets/img/image_12.png)

13. Una vez que estemos en la consola de PowerShell, entramos en la ruta **/site/wwwroot**

![/site/wwwroot](assets/img/image_13.png)

14. Introducir por teclado, el comando **wp --version** y pulsar la tecla intro. Si todo ha ido correctamente, a continuación, observaremos la versión de WordPress CLI que hemos instalado. Por ej, WP-CLI 2.6.0

![wp --version](assets/img/image_14.png)
